/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package associados.view;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author tallys
 */
@Entity
@Table(name = "associados", catalog = "fcc", schema = "")
@NamedQueries({
    @NamedQuery(name = "Associados.findAll", query = "SELECT a FROM Associados a")
    , @NamedQuery(name = "Associados.findByIdassociados", query = "SELECT a FROM Associados a WHERE a.idassociados = :idassociados")
    , @NamedQuery(name = "Associados.findByNome", query = "SELECT a FROM Associados a WHERE a.nome = :nome")
    , @NamedQuery(name = "Associados.findByEndereco", query = "SELECT a FROM Associados a WHERE a.endereco = :endereco")
    , @NamedQuery(name = "Associados.findByNumero", query = "SELECT a FROM Associados a WHERE a.numero = :numero")
    , @NamedQuery(name = "Associados.findByBairro", query = "SELECT a FROM Associados a WHERE a.bairro = :bairro")
    , @NamedQuery(name = "Associados.findByCidade", query = "SELECT a FROM Associados a WHERE a.cidade = :cidade")
    , @NamedQuery(name = "Associados.findByEstado", query = "SELECT a FROM Associados a WHERE a.estado = :estado")
    , @NamedQuery(name = "Associados.findByCep", query = "SELECT a FROM Associados a WHERE a.cep = :cep")
    , @NamedQuery(name = "Associados.findByCelular", query = "SELECT a FROM Associados a WHERE a.celular = :celular")
    , @NamedQuery(name = "Associados.findByTelefone", query = "SELECT a FROM Associados a WHERE a.telefone = :telefone")
    , @NamedQuery(name = "Associados.findByCpf", query = "SELECT a FROM Associados a WHERE a.cpf = :cpf")
    , @NamedQuery(name = "Associados.findByRg", query = "SELECT a FROM Associados a WHERE a.rg = :rg")
    , @NamedQuery(name = "Associados.findByPlano", query = "SELECT a FROM Associados a WHERE a.plano = :plano")})
public class Associados implements Serializable {

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "associados")
    private Dependentes_1 dependentes;

    @Transient
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idassociados")
    private Integer idassociados;
    @Basic(optional = false)
    @Column(name = "nome")
    private String nome;
    @Basic(optional = false)
    @Column(name = "endereco")
    private String endereco;
    @Column(name = "numero")
    private String numero;
    @Basic(optional = false)
    @Column(name = "bairro")
    private String bairro;
    @Basic(optional = false)
    @Column(name = "cidade")
    private String cidade;
    @Basic(optional = false)
    @Column(name = "estado")
    private String estado;
    @Column(name = "cep")
    private String cep;
    @Column(name = "celular")
    private String celular;
    @Column(name = "telefone")
    private String telefone;
    @Column(name = "cpf")
    private String cpf;
    @Column(name = "rg")
    private String rg;
    @Basic(optional = false)
    @Column(name = "plano")
    private String plano;

    public Associados() {
    }

    public Associados(Integer idassociados) {
        this.idassociados = idassociados;
    }

    public Associados(Integer idassociados, String nome, String endereco, String bairro, String cidade, String estado, String plano) {
        this.idassociados = idassociados;
        this.nome = nome;
        this.endereco = endereco;
        this.bairro = bairro;
        this.cidade = cidade;
        this.estado = estado;
        this.plano = plano;
    }

    public Integer getIdassociados() {
        return idassociados;
    }

    public void setIdassociados(Integer idassociados) {
        Integer oldIdassociados = this.idassociados;
        this.idassociados = idassociados;
        changeSupport.firePropertyChange("idassociados", oldIdassociados, idassociados);
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        String oldNome = this.nome;
        this.nome = nome;
        changeSupport.firePropertyChange("nome", oldNome, nome);
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        String oldEndereco = this.endereco;
        this.endereco = endereco;
        changeSupport.firePropertyChange("endereco", oldEndereco, endereco);
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        String oldNumero = this.numero;
        this.numero = numero;
        changeSupport.firePropertyChange("numero", oldNumero, numero);
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        String oldBairro = this.bairro;
        this.bairro = bairro;
        changeSupport.firePropertyChange("bairro", oldBairro, bairro);
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        String oldCidade = this.cidade;
        this.cidade = cidade;
        changeSupport.firePropertyChange("cidade", oldCidade, cidade);
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        String oldEstado = this.estado;
        this.estado = estado;
        changeSupport.firePropertyChange("estado", oldEstado, estado);
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        String oldCep = this.cep;
        this.cep = cep;
        changeSupport.firePropertyChange("cep", oldCep, cep);
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        String oldCelular = this.celular;
        this.celular = celular;
        changeSupport.firePropertyChange("celular", oldCelular, celular);
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        String oldTelefone = this.telefone;
        this.telefone = telefone;
        changeSupport.firePropertyChange("telefone", oldTelefone, telefone);
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        String oldCpf = this.cpf;
        this.cpf = cpf;
        changeSupport.firePropertyChange("cpf", oldCpf, cpf);
    }

    public String getRg() {
        return rg;
    }

    public void setRg(String rg) {
        String oldRg = this.rg;
        this.rg = rg;
        changeSupport.firePropertyChange("rg", oldRg, rg);
    }

    public String getPlano() {
        return plano;
    }

    public void setPlano(String plano) {
        String oldPlano = this.plano;
        this.plano = plano;
        changeSupport.firePropertyChange("plano", oldPlano, plano);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idassociados != null ? idassociados.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Associados)) {
            return false;
        }
        Associados other = (Associados) object;
        if ((this.idassociados == null && other.idassociados != null) || (this.idassociados != null && !this.idassociados.equals(other.idassociados))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "associados.view.Associados[ idassociados=" + idassociados + " ]";
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }

    public Dependentes_1 getDependentes() {
        return dependentes;
    }

    public void setDependentes(Dependentes_1 dependentes) {
        this.dependentes = dependentes;
    }
    
}
