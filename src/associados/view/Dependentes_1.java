/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package associados.view;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author tallys
 */
@Entity
@Table(name = "dependentes")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Dependentes_1.findAll", query = "SELECT d FROM Dependentes_1 d")
    , @NamedQuery(name = "Dependentes_1.findByAssociadosId", query = "SELECT d FROM Dependentes_1 d WHERE d.associadosId = :associadosId")
    , @NamedQuery(name = "Dependentes_1.findByDep1", query = "SELECT d FROM Dependentes_1 d WHERE d.dep1 = :dep1")
    , @NamedQuery(name = "Dependentes_1.findByDep2", query = "SELECT d FROM Dependentes_1 d WHERE d.dep2 = :dep2")
    , @NamedQuery(name = "Dependentes_1.findByDep3", query = "SELECT d FROM Dependentes_1 d WHERE d.dep3 = :dep3")
    , @NamedQuery(name = "Dependentes_1.findByDep4", query = "SELECT d FROM Dependentes_1 d WHERE d.dep4 = :dep4")
    , @NamedQuery(name = "Dependentes_1.findByDep5", query = "SELECT d FROM Dependentes_1 d WHERE d.dep5 = :dep5")
    , @NamedQuery(name = "Dependentes_1.findByDep6", query = "SELECT d FROM Dependentes_1 d WHERE d.dep6 = :dep6")
    , @NamedQuery(name = "Dependentes_1.findByDep7", query = "SELECT d FROM Dependentes_1 d WHERE d.dep7 = :dep7")
    , @NamedQuery(name = "Dependentes_1.findByDep8", query = "SELECT d FROM Dependentes_1 d WHERE d.dep8 = :dep8")
    , @NamedQuery(name = "Dependentes_1.findByDep9", query = "SELECT d FROM Dependentes_1 d WHERE d.dep9 = :dep9")
    , @NamedQuery(name = "Dependentes_1.findByDep10", query = "SELECT d FROM Dependentes_1 d WHERE d.dep10 = :dep10")})
public class Dependentes_1 implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "associados_id")
    private Integer associadosId;
    @Column(name = "dep1")
    private String dep1;
    @Column(name = "dep2")
    private String dep2;
    @Column(name = "dep3")
    private String dep3;
    @Column(name = "dep4")
    private String dep4;
    @Column(name = "dep5")
    private String dep5;
    @Column(name = "dep6")
    private String dep6;
    @Column(name = "dep7")
    private String dep7;
    @Column(name = "dep8")
    private String dep8;
    @Column(name = "dep9")
    private String dep9;
    @Column(name = "dep10")
    private String dep10;
    @JoinColumn(name = "associados_id", referencedColumnName = "idassociados", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Associados associados;

    public Dependentes_1() {
    }

    public Dependentes_1(Integer associadosId) {
        this.associadosId = associadosId;
    }

    public Integer getAssociadosId() {
        return associadosId;
    }

    public void setAssociadosId(Integer associadosId) {
        this.associadosId = associadosId;
    }

    public String getDep1() {
        return dep1;
    }

    public void setDep1(String dep1) {
        this.dep1 = dep1;
    }

    public String getDep2() {
        return dep2;
    }

    public void setDep2(String dep2) {
        this.dep2 = dep2;
    }

    public String getDep3() {
        return dep3;
    }

    public void setDep3(String dep3) {
        this.dep3 = dep3;
    }

    public String getDep4() {
        return dep4;
    }

    public void setDep4(String dep4) {
        this.dep4 = dep4;
    }

    public String getDep5() {
        return dep5;
    }

    public void setDep5(String dep5) {
        this.dep5 = dep5;
    }

    public String getDep6() {
        return dep6;
    }

    public void setDep6(String dep6) {
        this.dep6 = dep6;
    }

    public String getDep7() {
        return dep7;
    }

    public void setDep7(String dep7) {
        this.dep7 = dep7;
    }

    public String getDep8() {
        return dep8;
    }

    public void setDep8(String dep8) {
        this.dep8 = dep8;
    }

    public String getDep9() {
        return dep9;
    }

    public void setDep9(String dep9) {
        this.dep9 = dep9;
    }

    public String getDep10() {
        return dep10;
    }

    public void setDep10(String dep10) {
        this.dep10 = dep10;
    }

    public Associados getAssociados() {
        return associados;
    }

    public void setAssociados(Associados associados) {
        this.associados = associados;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (associadosId != null ? associadosId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Dependentes_1)) {
            return false;
        }
        Dependentes_1 other = (Dependentes_1) object;
        if ((this.associadosId == null && other.associadosId != null) || (this.associadosId != null && !this.associadosId.equals(other.associadosId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "associados.view.Dependentes_1[ associadosId=" + associadosId + " ]";
    }
    
}
